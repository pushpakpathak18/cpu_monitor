import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import oshi.SystemInfo;
import oshi.hardware.CentralProcessor;
import oshi.hardware.HardwareAbstractionLayer;

public class CPU_Performance
{
    private static Logger logger = LoggerFactory.getLogger(CPU_Performance.class);
    private static SystemInfo si = new SystemInfo();
    private static HardwareAbstractionLayer hal = si.getHardware();
    private static long[] prevTicks= new long[CentralProcessor.TickType.values().length];
    private static CentralProcessor cpu = hal.getProcessor();

    public static double getCPU()
    {
        double cpuLoad = cpu.getSystemCpuLoadBetweenTicks( prevTicks )* 100;
        prevTicks = cpu.getSystemCpuLoadTicks();
        System.out.println("cpuLoad : " + cpuLoad);
        return cpuLoad;
    }

    public static void main(String[] args) throws Exception{
        double tCPU = getCPU();
        logger.info(String.valueOf(tCPU));

    }
}
