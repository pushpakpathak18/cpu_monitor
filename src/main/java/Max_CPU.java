import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import oshi.SystemInfo;
import oshi.hardware.CentralProcessor;
import oshi.hardware.HardwareAbstractionLayer;
import java.io.*;

public class Max_CPU {
    public static float baseline = 6.5F;
   private static Logger logger = LoggerFactory.getLogger(CPU_Performance.class);
    private static SystemInfo si = new SystemInfo();
    private static HardwareAbstractionLayer hal = si.getHardware();
    private static long[] prevTicks= new long[CentralProcessor.TickType.values().length];
    private static CentralProcessor cpu = hal.getProcessor();
    public static float max;

    public static double getCPU()
    {
        double cpuLoad = cpu.getSystemCpuLoadBetweenTicks( prevTicks )* 100;
        prevTicks = cpu.getSystemCpuLoadTicks();
        System.out.println("Current CPU Load : " + cpuLoad);
        return cpuLoad;
    }
    public static float getMax(float baseL)
    {
        double tCPU = getCPU();
        if (tCPU > baseL)
           max = (float) tCPU;
        else
            max = baseL;
        return max;
    }

    public static void main(String[] args) throws Exception {
        try {
            float maxCPU;
            FileInputStream fstream = new FileInputStream("Peak_CPU.log");
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            if( br.readLine() == null)
            {
                maxCPU = getMax(baseline);
                System.out.println("Initial CPU cmp with Baseline(6.5) : " + maxCPU);
                logger.info(String.valueOf(maxCPU));
            }
            else {
                FileInputStream ffstream = new FileInputStream("Peak_CPU.log");
                BufferedReader brr = new BufferedReader(new InputStreamReader(ffstream));
                String strLine, last = null;
                while ((strLine = brr.readLine()) != null) {
                    if (strLine != null) {
                        last = strLine;
                    }
                }
                System.out.println("Last Value: " + last);
             //   float peak = 0;
                if (!last.isEmpty()) {
                    float cpuPeak = Float.parseFloat(last);
                    // System.out.println("Current: " + cpuPeak);
                    float peak = getMax(cpuPeak);
                    logger.info(String.valueOf(peak));
                    System.out.println("Updated Peak: " + peak);

                } else {
                    FileInputStream ffstream1 = new FileInputStream("Peak_CPU.log");
                    BufferedReader brr1 = new BufferedReader(new InputStreamReader(ffstream1));
                    String strLastLine = "";
                    String tmp;
                    String strlastLineMinusOne="";
                    while ((tmp = brr1.readLine()) != null)
                    {
                       strlastLineMinusOne = strLastLine;
                        strLastLine = tmp;
                    }
                    System.out.println("Last Line Minus one is : "+strlastLineMinusOne);
                    System.out.println("Last Line is : "+strLastLine);
                    logger.info(String.valueOf(strlastLineMinusOne));
                }
            }
            fstream.close();
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
    }
}