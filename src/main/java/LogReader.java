import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.FileInputStream;
import java.io.*;

class LogReader {
    static int i =0;
    static float baseline = 6.5F;
    private static Logger logger = LoggerFactory.getLogger(LogReader.class);
    public static void main(String argv[]) {
        try{
            FileInputStream fstream = new FileInputStream("CPU_Stats.log");
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            String strLine;
            float newBaseLine;
            String[] arr = new String[100];
            while((strLine = br.readLine()) != null) {
                arr[i++] = strLine;
            }
            newBaseLine=Float.parseFloat(arr[i-1]);
            System.out.println(" Last Value = = " +newBaseLine);
            float max;
            if(arr[24] != null)
            {
                baseline= newBaseLine;
                System.out.println(" Updated Baseline =  " +baseline);
                for (int j = 0; j < i; j++) {
                    max = Float.parseFloat(arr[j]);
                    if (baseline < max)
                        baseline = max;
                    if (j == 23) {
                        logger.info(String.valueOf(baseline));
                    }
                }
                System.out.print(" Max Value :::  " +baseline);
            }
            else
                {
                    System.out.println(" Old Baseline =  " +baseline);
                    for (int j = 0; j < i; j++) {
                        max = Float.parseFloat(arr[j]);
                        if (baseline < max)
                            baseline = max;
                        if (j == 23) {
                            logger.info(String.valueOf(baseline));
                        }
                    }
            }
            fstream.close();
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
    }
}